import glob
import os
from zonetool.test import *


EXPECTED_ZONE_DATA = '''
$TTL 3600
@   IN SOA localhost. hostmaster.example.com. (
    12345678 ; Serial is ignored in comparison
    43200
    3600
    2419200
    3600 )
    IN A 1.2.3.4
www IN CNAME a.b.c.d.
'''


class SimpleTest(Base):

    def test_simple_zone(self):
        results = self._run_test([
            {'example.com': {
                '_': '1.2.3.4',
                'www': 'CNAME a.b.c.d.',
            }},
        ])
        self.assertEqual(1, len(results))
        self.assertEqual('example.com', results[0][0])
        self.assertZoneEquals(EXPECTED_ZONE_DATA, results[0][2])

    def test_simple_zone_rendered(self):
        changed, removed = self._run_render_test([
            {'example.com': {
                '_': '1.2.3.4',
                'www': 'CNAME a.b.c.d.',
            }},
        ])
        self.assertTrue(changed)
        self.assertFalse(removed)

        zone_file = os.path.join(self.tmpdir, 'example.com')
        self.assertTrue(os.path.exists(zone_file))
        with open(zone_file) as f:
            zone_data = f.read()
            self.assertZoneEquals(EXPECTED_ZONE_DATA, zone_data)

    def test_dnssec_zone(self):
        changed, removed = self._run_dnssec_render_test([
            {'example.com': {
                'DNSSEC': True,
                '_': '1.2.3.4',
                'www': 'CNAME a.b.c.d.',
            }},
        ])
        self.assertTrue(changed)
        self.assertFalse(removed)

        zone_file = os.path.join(self.tmpdir, 'example.com')
        self.assertTrue(os.path.exists(zone_file))
        with open(zone_file) as f:
            zone_data = f.read()
            self.assertZoneEquals(EXPECTED_ZONE_DATA, zone_data)

        ksk_files = glob.glob(
            os.path.join(self.tmpdir, 'Kexample.com.+007+*.key'))
        self.assertTrue(ksk_files)

        signed_zone_file = os.path.join(self.tmpdir, 'example.com.signed')
        self.assertTrue(os.path.exists(signed_zone_file))

