import os
import shutil
import tempfile
import unittest

from zonetool import zone


class Base(unittest.TestCase):

    CONFIG = {}

    def setUp(self):
        self.tmpdir = tempfile.mkdtemp()
        self.zp = zone.ZoneParser(self.CONFIG)

    def tearDown(self):
        shutil.rmtree(self.tmpdir, ignore_errors=True)

    def _run_test(self, *zone_data):
        self.zp.load(zone_data)
        return list(self.zp.render())

    def _run_render_test(self, *zone_data):
        self.zp.load(zone_data)
        zw = zone.ZoneWriter(self.tmpdir,
                             os.path.join(self.tmpdir, 'named.conf'),
                             delete=True)
        return zw.write(self.zp.render())

    def _run_dnssec_render_test(self, *zone_data):
        self.zp.load(zone_data)
        pproc = zone.DNSSECSigner(
            self.tmpdir,
            self.tmpdir,
            '7',
            '53cd0ab9767f619d5377bdbcc8150c10',
            False, False)
        zw = zone.ZoneWriter(self.tmpdir,
                             os.path.join(self.tmpdir, 'named.conf'),
                             delete=True)
        return zw.write(self.zp.render(), postproc=pproc)

    def assertZoneEquals(self, zone_a, zone_b):
        self.assertTrue(
            zone.zonecmp(zone_a, zone_b),
            'Zones do not match:\n\nexpected:\n%s\n\ngot:\n%s' % (zone_a, zone_b))
