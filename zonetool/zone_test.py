import yaml
import shutil
import tempfile
import unittest
from io import StringIO
from zonetool.zone import *


class TestMergeZones(unittest.TestCase):

    def test_merge_zones(self):
        a = {'_': ['1.2.3.4']}
        b = {'_': ['TXT spf ...'], 'EXTENDS': ['a']}
        c = {'_': ['2.3.4.5'], 'REPLACES': ['a']}
        zp = ZoneParser()
        zp.zones.update({'a': a, 'b': b, 'c':  c})
        result = zp._resolve_references('@b', b)
        self.assertEqual(set(['1.2.3.4', 'TXT spf ...']), set(result['_']))
        result = zp._resolve_references('@c', c)
        self.assertEqual(['2.3.4.5'], result['_'])


TEST_DATA = [
    '''
autistici.org:
  DNSSEC: true
  EXTENDS:
    - "@default"
  _:
    - 82.94.249.234
    - 82.221.99.153
  www: "$FRONTENDS"
''',
    '''
"@default":
  _:
    - NS ns1.autistici.org.
    - NS ns2.autistici.org.
''',
    '''
"@default":
  onion:
    - TXT "blahblah.onion."
''',
]

TEST_DATA_2 = [
    '''
autistici.org:
  www:
    - 82.94.249.234
    - 82.221.99.153
''', '''
autistici.org:
  www:
    - TXT "web"
''', '''
autistici.org:
  www:
    - 2a02:f48:2000:201::19
    - 2002:b2ff:9023::1
''',
]

TEST_DATA_3 = [
    '''
autistici.org:
  _:
    - NS $FRONTENDS
    - MX 10 $FRONTENDS
    - 82.94.249.234
    - 82.221.99.153
''',
]

TEST_DATA_4 = [
    '''
autistici.org:
  EXTENDS:
    - "@default"
  _:
    - 1.1.1.3
  www: CNAME "www.l.autistici.org"
''',
    '''
"@default":
  EXTENDS:
    - "@base"
  _:
    - 1.1.1.2
    - NS ns2.autistici.org.
''',
    '''
"@base":
  _:
    - 1.1.1.1
    - MX 10 $FRONTENDS
    - NS ns1.autistici.org.
''',
]

TEST_DATA_5 = [
    '''
noblogs.org:
  DNSSEC: "true"
  EXTENDS: "@default"
  "*": CNAME www.autistici.org.
  stats: CNAME stats.autistici.org.
  _D9A8E643F8F825D3B4AD63EAC80754B5: CNAME FB7700192E2FC9957FA8F0257E8C97A6.4D6E7C43C30CFFFBC12BC64153BBB6E8.5801d49c965a32a5303c.comodoca.com.
''',
]

TEST_DATA_EMPTY_VAR = [
    '''
autistici.org:
  banana:
    - MX 10 $EMPTY
''',
]

TEST_CONFIG = {
    'FRONTENDS': ['82.94.249.234', '82.221.99.153'],
    'EMPTY': [],
}


def _loadyaml(strs):
    for s in strs:
        yield yaml.safe_load_all(StringIO(s))


class ZoneParserTest(unittest.TestCase):

    def setUp(self):
        self.zp = ZoneParser(TEST_CONFIG)

    def test_template_expansion(self):
        self.zp.load(_loadyaml(TEST_DATA))
        result = list(self.zp.render())

        self.assertTrue(result)
        self.assertEqual('autistici.org', result[0][0])

        # Verify that $FRONTENDS has been replaced correctly.
        self.assertTrue(isinstance(result[0][1]['www'], list))
        self.assertEqual(
            sorted(result[0][1]['www']),
            sorted(TEST_CONFIG['FRONTENDS']))

    def test_zone_output(self):
        self.zp.load(_loadyaml(TEST_DATA))
        result = list(self.zp.render())
        zone_data = result[0][2]
        expected_data = '''; Comments should be ignored
$TTL 3600
@		IN	SOA	ns1.autistici.org.	hostmaster.autistici.org. (
				1521885904 ; Serial
				43200
				3600
				2419200
				3600 )
		IN	A	82.221.99.153
		IN	A	82.94.249.234
		IN	NS	ns1.autistici.org.
		IN	NS	ns2.autistici.org.
onion		IN	TXT	"blahblah.onion."
www		IN	A	82.221.99.153
www		IN	A	82.94.249.234
'''
        self.assertTrue(
            zonecmp(zone_data, expected_data),
            'Bad zone data: got: %s, expected: %s' % (
                zone_data, expected_data))

    def test_merge(self):
        self.zp.load(_loadyaml(TEST_DATA_2))
        result = list(self.zp.render())

        self.assertTrue(result)
        self.assertEqual('autistici.org', result[0][0])

        # Verify that all 'www' entries have been merged properly.
        www = set(result[0][1]['www'])
        expected = set(['82.94.249.234', '82.221.99.153', 'TXT "web"',
                        '2a02:f48:2000:201::19', '2002:b2ff:9023::1'])
        self.assertEqual(expected, www)

    def test_expand_list_keywords(self):
        self.zp.load(_loadyaml(TEST_DATA_3))
        result = list(self.zp.render())
        self.assertTrue(result)
        self.assertEqual('autistici.org', result[0][0])

        # Count the number of NS records.
        num_ns = len([x for x in result[0][1]['_'] if x.startswith('NS ')])
        self.assertEqual(num_ns, 2)

    def test_expand_empty_variable(self):
        self.zp.load(_loadyaml(TEST_DATA_EMPTY_VAR))
        result = list(self.zp.render())
        self.assertTrue(result)
        self.assertEqual('autistici.org', result[0][0])

        # The record that expanded an empty list should not be
        # present at all in the zone.
        self.assertTrue('banana' not in result[0][1],
                        'Bad zone with empty record:\n%s' % result[0][1])

    def test_recursive_extend(self):
        self.zp.load(_loadyaml(TEST_DATA_4))
        result = list(self.zp.render())
        self.assertTrue(result)
        for frontend in TEST_CONFIG['FRONTENDS']:
            self.assertIn('MX 10 %s' % frontend, result[0][1]['_'])
        self.assertEqual('autistici.org', result[0][0])

    def test_uppercase_entry(self):
        self.zp.load(_loadyaml(TEST_DATA_5))
        result = list(self.zp.render())
        self.assertTrue(result)
        self.assertTrue('_D9A8E643F8F825D3B4AD63EAC80754B5' in result[0][1], result[0][1])
        self.assertTrue('_D9A8E643F8F825D3B4AD63EAC80754B5' in result[0][2], result[0][2])


class ZoneWriterTestBase(unittest.TestCase):

    def setUp(self):
        self.zp = ZoneParser(TEST_CONFIG)
        self.tmpdir = tempfile.mkdtemp()
        zonedir = os.path.join(self.tmpdir, 'zones')
        os.mkdir(zonedir)
        self.zw = ZoneWriter(zonedir,
                             os.path.join(self.tmpdir, 'named.conf'))

    def tearDown(self):
        shutil.rmtree(self.tmpdir)


class ZoneWriterTest(ZoneWriterTestBase):

    def test_write(self):
        self.zp.load(_loadyaml(TEST_DATA))
        changed, removed = self.zw.write(self.zp.render())
        self.assertEqual(set(['autistici.org']), changed)
        self.assertEqual(set(), removed)
        self.assertTrue(
            os.path.exists(os.path.join(self.tmpdir, 'named.conf')))

        # Writing a second time, should see no changes.
        changed, removed = self.zw.write(self.zp.render())
        self.assertEqual(set(), changed)
        self.assertEqual(set(), removed)

        # Change the global config, render again.
        cfg = dict(TEST_CONFIG)
        cfg['FRONTENDS'][0] = '178.255.144.35'
        zp2 = ZoneParser(cfg)
        zp2.load(_loadyaml(TEST_DATA))
        changed, removed = self.zw.write(zp2.render())
        self.assertEqual(set(['autistici.org']), changed)
        self.assertEqual(set(), removed)

        # Replace a domain with another, triggering a change and a
        # removal.
        zp3 = ZoneParser(TEST_CONFIG)
        data = [TEST_DATA[0].replace('autistici.org', 'inventati.org')]
        zp3.load(_loadyaml(data))
        changed, removed = self.zw.write(zp3.render())
        self.assertEqual(set(['inventati.org']), changed)
        self.assertEqual(set(['autistici.org']), removed)

    def test_config(self):
        self.zp.load(_loadyaml(TEST_DATA))
        self.zw.write(self.zp.render())

        with open(os.path.join(self.tmpdir, 'named.conf')) as fd:
            named_conf = fd.read()
        self.assertEqual(named_conf, '''
zone "autistici.org" {
  type master;
  file "%s/zones/autistici.org";
  allow-query { any; };
};
''' % (self.tmpdir,))

    def test_config_with_update_policies(self):
        self.zp.load(_loadyaml(TEST_DATA))
        self.zw.update_policies = [
            'grant testkey zonesub',
            'grant acme zonesub TXT',
        ]
        self.zw.write(self.zp.render())

        with open(os.path.join(self.tmpdir, 'named.conf')) as fd:
            named_conf = fd.read()
        self.assertEqual(named_conf, '''
zone "autistici.org" {
  type master;
  file "%s/zones/autistici.org";
  allow-query { any; };
  update-policy {
    grant testkey zonesub;
    grant acme zonesub TXT;
  };
};
''' % (self.tmpdir,))


class DNSSECTest(ZoneWriterTestBase):

    def setUp(self):
        ZoneWriterTestBase.setUp(self)

        key_dir = os.path.join(self.tmpdir, 'keys')
        ds_dir = os.path.join(self.tmpdir, 'ds')
        os.mkdir(key_dir)
        os.mkdir(ds_dir)
        self.signer = DNSSECSigner(key_dir, ds_dir, '7', '123456', False)

    def test_sign_zone(self):
        self.zp.load(_loadyaml(TEST_DATA))
        self.zw.write(self.zp.render(), postproc=self.signer)

        self.assertTrue(self.signer.has_keys('autistici.org'))

        # Test signing again, but add a tripwire to the signing
        # function to verify it isn't called.
        def oh_no(*args):
            raise Exception('called sign_zone_file')
        self.signer.sign_zone_file = oh_no
        self.zw.write(self.zp.render(), postproc=self.signer)
